package message

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
)

var metapath string

type MetaFiles struct {
	Last  uint64   `json:"last"`
	Path  string   `json:"path"`
	Files []string `json:"file"`
}

/*
type MetaM struct {
	Owner    string    `json:"owner"`
	Pass     string    `json:"pass"`
	Messages []Message `json:"messages"`
}

type Message struct {
	To        string `json:"to"`
	From      string `json:"from"`
	Subject   string `json:"subject"`
	Body      string `json:"body"`
	Timestamp int64  `json:"timestamp"`
}
*/
func NewMetaFiles(path string) *MetaFiles {
	metapath = path
	f, err := os.Open(path)
	if err != nil {
		panic(err)
	}
	raw, err := ioutil.ReadAll(f)
	if err != nil {
		panic(err)
	}

	m := MetaFiles{}
	err = json.Unmarshal(raw, &m)
	if err != nil {
		fmt.Println(err)
	}

	return &m

}

func (m *MetaFiles) Create(data string) error {
	m.Last++
	newf := "m" + strconv.FormatUint(m.Last, 10)
	newpath := m.Path + newf
	err := ioutil.WriteFile(newpath, []byte(data), 0644)
	if err != nil {
		panic(err)
	}

	m.Files = append(m.Files, newf)
	return nil
}
func (m *MetaFiles) Read(mfile string) ([]byte, error) {
	data := []byte{}
	if _, err := os.Stat(m.Path + mfile); os.IsNotExist(err) {
		return data, err
	}
	f, err := os.Open(m.Path + mfile)
	if err != nil {
		fmt.Println(err)
		return data, err
	}
	data, err = ioutil.ReadAll(f)
	if err != nil {
		fmt.Println(err)
		return data, err
	}

	return data, nil
}

func (m *MetaFiles) Delete(name string) error {
	for i, val := range m.Files {
		if val == name {
			copy(m.Files[i:], m.Files[i+1:])
			m.Files[len(m.Files)-1] = ""
			m.Files = m.Files[:len(m.Files)-1]
			break
		}
	}
	if _, err := os.Stat(m.Path + name); os.IsNotExist(err) {
		return err
	}
	err := os.Remove(m.Path + name)
	if err != nil {
		return err
	}

	return nil
}

func (m *MetaFiles) Update() error {
	j, err := json.Marshal(m)
	if err != nil {
		fmt.Println(err)
	}

	err = ioutil.WriteFile(metapath, j, 0644)
	if err != nil {
		fmt.Println(err)
	}
	return nil
}
